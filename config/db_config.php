<?php
try {
    $db = new PDO('mysql:host=localhost;dbname=person', 'admin', '11111');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec('SET NAMES "utf8"');
} catch (Exception $e) {
    $message = 'Technical problems, please come back later :-) ' . $e->getMessage();
    die($message);
}
?>