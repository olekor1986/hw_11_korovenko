<?php
require_once "config/db_config.php";
require_once 'Classes/Person.php';
require_once 'Classes/Student.php';
require_once 'Classes/Teacher.php';
require_once 'Classes/Admin.php';

$id = htmlspecialchars(stripslashes(trim($_POST['personId'])), ENT_QUOTES, "UTF-8");
$full_name = htmlspecialchars(stripslashes(trim($_POST['full_name'])), ENT_QUOTES, "UTF-8");
$phone = htmlspecialchars(stripslashes(trim($_POST['phone'])), ENT_QUOTES, "UTF-8");
$email = htmlspecialchars(stripslashes(trim($_POST['email'])), ENT_QUOTES, "UTF-8");
$role = htmlspecialchars(stripslashes(trim($_POST['role'])), ENT_QUOTES, "UTF-8");

if ($role == 'student') {
    $property = htmlspecialchars(stripslashes(trim($_POST['average_mark'])), ENT_QUOTES, "UTF-8");
    $updateObject = new Student ($id, $full_name, $phone, $email, $role, $property);
} else if ($role == 'teacher') {
    $property = htmlspecialchars(stripslashes(trim($_POST['subject'])), ENT_QUOTES, "UTF-8");
    $updateObject = new Teacher ($id, $full_name, $phone, $email, $role, $property);
} else {
    $property = htmlspecialchars(stripslashes(trim($_POST['working_day'])), ENT_QUOTES, "UTF-8");
    $updateObject = new Admin ($id, $full_name, $phone, $email, $role, $property);
}

try {
        $sql = "UPDATE members SET
            full_name = :full_name,
            phone = :phone,
            email = :email,
            role = :role,
            average_mark = :average_mark,
            subject = :subject,
            working_day = :working_day
            WHERE id = :id";
    $updatePerson = $db->prepare($sql);
    $updatePerson->bindValue(':id', $updateObject->getId());
    $updatePerson->bindValue(':full_name', $updateObject->getFullName());
    $updatePerson->bindValue(':phone', $updateObject->getPhone());
    $updatePerson->bindValue(':email', $updateObject->getEmail());
    $updatePerson->bindValue(':role', $updateObject->getRole());
        if ($updateObject->getRole() == 'student'){
            $updatePerson->bindValue(':average_mark', $updateObject->getAverageMark());
        } else {
            $updatePerson->bindValue(':average_mark', NULL);
        }
        if ($updateObject->getRole() == 'teacher'){
            $updatePerson->bindValue(':subject',  $updateObject->getSubject());
        } else {
            $updatePerson->bindValue(':subject',  NULL);
        }
        if ($updateObject->getRole() == 'teacher'){
            $updatePerson->bindValue(':working_day', $updateObject->getWorkingDay());
        } else {
            $updatePerson->bindValue(':working_day', NULL);
        }
        $updatePerson->execute();
} catch (Exception $e) {
    $message = 'Error update person!' . $e->getMessage();
    die($message);
}
header('Location:showList.php');
?>