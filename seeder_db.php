<?php
require_once "config/db_config.php";

$personArray = [
    [
        'full_name' => 'MARIA FALABELLA',
        'phone' => '547-85-67',
        'email' => 'maria@gmail.com',
        'role' => 'student',
        'average_mark' => '6.5'
    ],
    [
        'full_name' => 'DENISE BUREN',
        'phone' => '213-56-21',
        'email' => 'denise@gmail.com',
        'role' => 'student',
        'average_mark' => '9.7'
    ],
    [
        'full_name' => 'BARBARA DIMASI',
        'phone' => '325-95-85',
        'email' => 'barbara@mail.com',
        'role' => 'student',
        'average_mark' => '8.5'
    ],
    [
        'full_name' => 'JESSE SWEENEY',
        'phone' => '123-33-33',
        'email' => 'jesse@mail.com',
        'role' => 'student',
        'average_mark' => '5.5'
    ],
    [
        'full_name' => 'JACKIE RASEMAN',
        'phone' => '167-75-62',
        'email' => 'jackie@mail.com',
        'role' => 'teacher',
        'subject' => 'Java'
    ],
    [
        'full_name' => 'LAURA CASTELLI',
        'phone' => '452-65-89',
        'email' => 'laura@mail.com',
        'role' => 'teacher',
        'subject' => 'HTML'
    ],
    [
        'full_name' => 'MICHAEL J. AMODIO',
        'phone' => '697-48-31',
        'email' => 'michael@mail.com',
        'role' => 'teacher',
        'subject' => 'JavaScript'
    ],
    [
        'full_name' => 'ANGELA ROBERTS',
        'phone' => '326-98-78',
        'email' => 'angela@mail.com',
        'role' => 'admin',
        'working_day' => 'monday'
    ],
    [
        'full_name' => 'JOAN HARTE',
        'phone' => '145-65-77',
        'email' => 'joan@mail.com',
        'role' => 'admin',
        'working_day' => 'thursday'
    ],
    [
        'full_name' => 'SHELBIE ELY',
        'phone' => '145-65-77',
        'email' => 'shelbie@mail.com',
        'role' => 'admin',
        'working_day' => 'saturday'
    ]
];

try {
    foreach ($personArray as $key => $value) {
        $sql = "INSERT INTO members SET
        full_name = :full_name,
        phone = :phone,
        email = :email,
        role = :role,
        average_mark = :average_mark,
        subject = :subject,
        working_day = :working_day";
        if (!isset($value['average_mark'])){
            $value['average_mark'] = NULL;
        }
        if (!isset($value['subject'])){
            $value['subject'] = NULL;
        }
        if (!isset($value['working_day'])){
            $value['working_day'] = NULL;
        }
        $seederObject = $db->prepare($sql);
        $seederObject->bindValue(':full_name', $value['full_name']);
        $seederObject->bindValue(':phone', $value['phone']);
        $seederObject->bindValue(':email', $value['email']);
        $seederObject->bindValue(':role', $value['role']);
        $seederObject->bindValue(':average_mark', $value['average_mark']);
        $seederObject->bindValue(':subject', $value['subject']);
        $seederObject->bindValue(':working_day',  $value['working_day']);
        $seederObject->execute();
    }
} catch (Exception $e) {
    $message = 'Error create Person!' . $e->getMessage();
    die($message);
}
header('Location:showList.php');
?>