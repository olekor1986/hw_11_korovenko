<?php
require_once "config/db_config.php";

$id = htmlspecialchars(stripslashes(trim($_POST['personID'])), ENT_QUOTES, "UTF-8");

try {
    $sql = "DELETE FROM members WHERE id = :id";
    $deletePerson = $db->prepare($sql);
    $deletePerson->bindValue(':id', $id);
    $deletePerson->execute();
} catch (Exception $e) {
    $message = 'Error deleting person! ' . $e->getMessage();
    die($message);
}
header('Location:showList.php');
?>