<?php
require_once "config/db_config.php";
require_once 'Classes/Person.php';
require_once 'Classes/Student.php';
require_once 'Classes/Teacher.php';
require_once 'Classes/Admin.php';

try {
    $sql = 'SELECT * FROM members';
    $result = $db->query($sql);
    $personArray = $result->fetchAll();
} catch(Exception $e) {
    $message = 'Error getting data! ' . $e->getMessage();
    die($message);
}

$allPersonObject = [];
foreach ($personArray as $key => $value) {
    if ($value['role'] == 'student') {
        $allPersonsObject[] = new Student ($value['id'], $value['full_name'], $value['phone'], $value['email'],
            $value['role'], $value['average_mark']);
    } else if ($value['role'] == 'teacher') {
        $allPersonsObject[] = new Teacher ($value['id'], $value['full_name'], $value['phone'], $value['email'],
            $value['role'], $value['subject']);
    } else {
        $allPersonsObject[] = new Admin ($value['id'], $value['full_name'], $value['phone'], $value['email'],
            $value['role'], $value['working_day']);
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Members List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="container header text-center">
        <h2>Members List</h2>
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Main Page</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="showList.php">Members List</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="seeder_db.php">Add person</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="create_db.php">Create database</a>
            </li>
        </ul>
    </div>
    <div class="container content">
        <?php foreach ($allPersonsObject as $key => $value) :?>
        <div class="card">
            <div class="card-body">
               <p class="card-text"><?=$allPersonsObject[$key]->getVisitCard()?></p>
                    <form action="editPerson.php" method="post">
                    <input type="hidden" name="personId" value="<?=$allPersonsObject[$key]->getId()?>">
                    <button class="btn btn-outline-info">Edit</button>
                </form>
                <form action="deletePerson.php" method="post">
                    <input type="hidden" name="personID" value="<?=$allPersonsObject[$key]->getId()?>">
                    <button class="btn btn-outline-danger">Delete</button>
                </form>
            </div>
        </div>
        <?php endforeach;?>
    </div>
    <div class="container footer text-center">
        <p>University (c) 2019</p>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>