<?php
require_once "config/db_config.php";
require_once 'Classes/Person.php';
require_once 'Classes/Student.php';
require_once 'Classes/Teacher.php';
require_once 'Classes/Admin.php';

$id = htmlspecialchars(stripslashes(trim($_POST['personId'])), ENT_QUOTES, "UTF-8");

try {
    $sql = "SELECT * FROM members WHERE id = :id";
    $selectedPerson = $db->prepare($sql);
    $selectedPerson->bindValue(':id', $id);
    $selectedPerson->execute();
    $person = $selectedPerson->fetch();
} catch (Exception $e) {
    $message = 'Error from editing person!' . $e->getMessage();
    die($message);
}
if ($person['role'] == 'student') {
    $editPersonObject = new Student ($person['id'], $person['full_name'], $person['phone'], $person['email'],
        $person['role'], $person['average_mark']);
} else if ($person['role'] == 'teacher') {
    $editPersonObject = new Teacher ($person['id'], $person['full_name'], $person['phone'], $person['email'],
        $person['role'], $person['subject']);
} else {
    $editPersonObject = new Admin ($person['id'], $person['full_name'], $person['phone'], $person['email'],
        $person['role'], $person['working_day']);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Edit Person</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container header text-center">
    <h2>Edit Person</h2>
    <ul class="nav justify-content-center">
        <li class="nav-item">
            <a class="nav-link active" href="index.php">Main Page</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="showList.php">Members List</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="seeder_db.php">Add person</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="create_db.php">Create database</a>
        </li>
    </ul>
</div>
<div class="container content">
    <form action="updatePerson.php" method="post">
        <div class="form-group">
            <label for="validationCustom01">Full Name
                <input type="text" class="form-control" id="validationCustom01" required name="full_name" value="<?=$editPersonObject->getFullName()?>">
            </label>
        </div>
        <div class="form-group">
            <label for="validationCustom02">Phone
                <input type="phone" type="text" class="form-control" id="validationCustom02" required name="phone" value="<?=$editPersonObject->getPhone()?>">
            </label>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">E-mail
                <input type="email" type="text" class="form-control" id="exampleInputEmail1" required aria-describedby="emailHelp" name="email" value="<?=$editPersonObject->getEmail()?>">
            </label>
        </div>
        <div class="form-group">
            <label for="validationCustom03">Role
                <input type="text" type="text" class="form-control" id="validationCustom03" required name="role" value="<?=$editPersonObject->getRole()?>">
            </label>
        </div>
        <div class="form-group">
            <?php if ($editPersonObject->getRole() == 'student'):?>
            <label for="validationCustom04">Average Mark
                <input type="text" type="text" class="form-control" id="validationCustom04" required name="average_mark" value="<?=$editPersonObject->getAverageMark()?>">
            </label>
            <?php endif;?>
            <?php if ($editPersonObject->getRole() == 'teacher'):?>
                <label for="validationCustom04">Subject
                    <input type="text" type="text" class="form-control" id="validationCustom04" required name="subject" value="<?=$editPersonObject->getSubject()?>">
                </label>
            <?php endif;?>
            <?php if ($editPersonObject->getRole() == 'admin'):?>
                <label for="validationCustom04">Working Day
                    <input type="text" type="text" class="form-control" id="validationCustom04" required name="working_day" value="<?=$editPersonObject->getWorkingDay()?>">
                </label>
            <?php endif;?>
        </div>
        <input type="hidden" name="personId" value="<?=$editPersonObject->getId()?>">
        <button class="btn btn-outline-primary">Save Person</button>
    </form>
</div>
<div class="container footer text-center">
    <p>University (c) 2019</p>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
