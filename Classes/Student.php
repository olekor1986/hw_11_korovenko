<?php


class Student extends Person
{
    protected $average_mark;

    public function __construct($id, $full_name, $phone, $email, $role, $average_mark)
    {
        parent::__construct($id, $full_name, $phone, $email, $role);

        $this->average_mark = $average_mark;

    }

    /**
     * return Average mark
     */
    public function getAverageMark()
    {
        return $this->average_mark;
    }
    /**
     * return all data as string and average mark
     */
    public function getVisitCard()
    {
        $result = parent::getVisitCard();
        $result .= $this->average_mark;
        return $result;
    }
}