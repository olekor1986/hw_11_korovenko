<?php


class Person
{
    protected $id;
    protected $full_name;
    protected $phone;
    protected $email;
    protected $role;

    public function __construct($id, $full_name, $phone, $email, $role)
    {
        $this->id = $id;
        $this->full_name = $full_name;
        $this->phone = $phone;
        $this->email = $email;
        $this->role = $role;
    }

    /**
     * return ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * return Full Name
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * return Phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * return Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * return all data as string
     */
    public function getVisitCard()
    {
        $result = '';
        $result .= $this->full_name . ', ';
        $result .= $this->phone . ', ';
        $result .= $this->email . ', ';
        $result .= $this->role . ', ';
        return $result;
    }
}