<?php


class Admin extends Person
{
    protected $working_day;

    public function __construct($id, $full_name, $phone, $email, $role, $working_day)
    {
        parent::__construct($id, $full_name, $phone, $email, $role);

        $this->working_day = $working_day;
    }

    /**
     * return working day
     */
    public function getWorkingDay()
    {
        return $this->working_day;
    }
    /**
     * return all data as string and working day
     */
    public function getVisitCard()
    {
        $result = parent::getVisitCard();
        $result .= $this->working_day;
        return $result;
    }
}