<?php


class Teacher extends Person
{
    protected $subject;

    public function __construct($id, $full_name, $phone, $email, $role, $subject)
    {
        parent::__construct($id, $full_name, $phone, $email, $role);

        $this->subject = $subject;
    }

    /**
     * return subject
     */
    public function getSubject()
    {
        return $this->subject;
    }
    /**
     * return all data as string and subject
     */
    public function getVisitCard()
    {
        $result = parent::getVisitCard();
        $result .= $this->subject;
        return $result;
    }
}