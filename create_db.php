<?php
require_once "config/db_config.php";

try {
    $sql = 'CREATE TABLE members (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            full_name VARCHAR(255),
            phone VARCHAR(255),
            email VARCHAR(255),
            role VARCHAR(255),
            average_mark FLOAT,
            subject VARCHAR(255),
            working_day VARCHAR(255)
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
    $db->exec($sql);
} catch (Exception $e) {
    $message = 'Error creating database table! ' . $e->getMessage();
    die($message);
}
header("Location:index.php");
?>
